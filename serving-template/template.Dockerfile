FROM python:3-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

COPY ./server.py /usr/src/app
ENV FLASK_APP=server.py

# Expose the Flask port
EXPOSE 5000

ENTRYPOINT [ "flask","run","--host=0.0.0.0"]
