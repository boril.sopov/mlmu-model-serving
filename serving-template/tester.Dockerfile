FROM docker:latest

RUN apk add --update python python-dev curl libgcc bash docker-compose

CMD [ "docker-compose", "-version" ]