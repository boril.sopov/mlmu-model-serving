#!flask/bin/python
from flask import Flask, jsonify, request, json
from flask_cors import CORS
import sys
import os
import pickle
import json

MODEL_NAME = os.environ["MODEL_NAME"]

app = Flask(__name__)
CORS(app)

with open(os.path.join("/data",os.environ["MODEL_NAME"]), 'rb') as f:
    weights = pickle.load(f)

@app.route('/')
def index():
    return "Welcome to our ML-MU api server!\n"

@app.route('/calculate', methods=['POST'])
def calculate():
    content = json.loads(request.get_data())
    return jsonify({
            "mgs": "all good",
            "response": weights[0] * float(content["x"]) + weights[1] * float(content["y"]),
            "model_name": MODEL_NAME,
            })

if __name__ == '__main__':
    app.run()
