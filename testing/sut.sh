#!/bin/bash

echo "Running the test ...after 1s"

sleep 1s

curl -s http://app:5000/

if [ $? = "0" ]; then
    echo "Test passed"
    # exit 0
else
    echo "Test did not pass"
    exit 1
fi


curl -s -XPOST http://app:5000/calculate -d '{"x":1,"y":6}'

if [ $? = "0" ]; then
    echo "Test passed"
    exit 0
else
    echo "Test did not pass"
    exit 1
fi