FROM alpine

RUN apk add --no-cache bash curl

RUN mkdir /app
COPY sut.sh /app

ENTRYPOINT [ "bash", "/app/sut.sh" ]