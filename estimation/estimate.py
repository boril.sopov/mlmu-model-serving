import os
import sys
import pickle

if __name__ == '__main__':
    print("Starting the estimation...")

    # some model weights
    a = 10
    b = 1000

    # save the weights
    job_id = os.environ['CI_COMMIT_SHORT_SHA']
    filename = f'weights_v{job_id}.dat'
    with open(filename, 'wb') as f:
        pickle.dump((a,b),f)

    print(f"Weight calculated: {filename}")
