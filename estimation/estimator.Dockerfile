FROM python:3-alpine

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY . /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt

ARG CI_COMMIT_SHORT_SHA
ARG AWS_BUCKET
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY

RUN python /usr/src/app/estimate.py

## COPY files TO AWS
RUN aws s3 cp ./weights_v${CI_COMMIT_SHORT_SHA}.dat s3://$AWS_BUCKET/weights_v${CI_COMMIT_SHORT_SHA}.dat