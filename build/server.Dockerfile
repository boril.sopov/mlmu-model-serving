FROM python:3-alpine as downloader

RUN pip install awscli

RUN mkdir /data
WORKDIR /data

ARG CI_COMMIT_SHORT_SHA
ARG AWS_BUCKET
ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY

## COPY files from AWS
RUN aws s3 cp s3://$AWS_BUCKET/weights_v${CI_COMMIT_SHORT_SHA}.dat .

FROM borilsopov/template:latest

ARG CI_COMMIT_SHORT_SHA
ENV MODEL_NAME=weights_v${CI_COMMIT_SHORT_SHA}.dat

RUN mkdir /data

# from downloader -> copy files to template
COPY --from=downloader /data/weights_v${CI_COMMIT_SHORT_SHA}.dat /data
